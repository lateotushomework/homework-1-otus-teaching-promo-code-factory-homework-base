﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }

        public Task<List<T>> GetAllAsync() => Task.FromResult(Data);

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T entity) => Task.Run(() => Data.Add(entity));

        public Task UpdateAsync(T entity) => Task.Run(() =>
        {
            DeleteAsync(entity.Id);
            CreateAsync(entity);
        });

        public Task DeleteAsync(Guid id) => Task.Run(() => Data.RemoveAll(x => x.Id == id));
    }
}